import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CuadroBusquedaComponent } from './cuadro-busqueda.component';

describe('CuadroBusquedaComponent', () => {
  let component: CuadroBusquedaComponent;
  let fixture: ComponentFixture<CuadroBusquedaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CuadroBusquedaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CuadroBusquedaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
