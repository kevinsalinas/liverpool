import { Component, OnInit } from '@angular/core';
import { LiverpoolSearchService } from '../../services/liverpool-search.service';

@Component({
  selector: 'app-cuadro-busqueda',
  templateUrl: './cuadro-busqueda.component.html',
  styles: []
})
export class CuadroBusquedaComponent {

  termino: String = null;
  productos: any = null;
  fade: String = 'animated fadeIn';
  busquedas: String = null;
  items: any[] = [];

  constructor(public search: LiverpoolSearchService) {
    this.cargarLista();
  }

  limpiar(){
    if (this.termino == (null || '')) {
      this.productos = null;
    }
  }

  cargarLista(){
    // sessionStorage.removeItem('busquedas');
    this.busquedas = sessionStorage.getItem('busquedas');
    if (this.busquedas!=null) {
      this.items = this.busquedas.split(',');
      console.log(this.items);
    }
  }

  cambia() {
    console.log(this.fade);
    console.log(this.termino);
    this.fade = 'animated fadeOut';
  }

  enviar() {
    this.search.getBusqueda(this.termino).subscribe((data: any) => {
      this.productos = data;
      if (window.sessionStorage) {
        this.busquedas = sessionStorage.getItem('busquedas');
        if (this.busquedas != null) {
          let b = this.busquedas + ',' + this.termino;
          sessionStorage.setItem('busquedas', b);
          this.cargarLista();
        }else{
          let b = this.termino.toString();
          console.log(b);
          sessionStorage.setItem('busquedas',b);
          this.cargarLista();
        }
      }else{
        console.log('session storage no soportado');
      }
    });
  }
}
