import { TestBed, inject } from '@angular/core/testing';

import { LiverpoolSearchService } from './liverpool-search.service';

describe('LiverpoolSearchService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LiverpoolSearchService]
    });
  });

  it('should be created', inject([LiverpoolSearchService], (service: LiverpoolSearchService) => {
    expect(service).toBeTruthy();
  }));
});
