import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()
export class LiverpoolSearchService {

  productos: any[] = [];

  constructor(public http: HttpClient) {}


  getBusqueda(termino: String) {
    let url = `https://www.liverpool.com.mx/tienda/?s=${termino}&d3106047a194921c01969dfdec083925=json`;
    return this.http.get(url).map( (data: any) => {
              this.productos = data.contents[0].mainContent[3].contents[0].records;
              return this.productos;
            });
  }
}
