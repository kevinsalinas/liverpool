# Demo Liverpool

## requerimientos

 * Tener instalado nodejs y npm
 * instalar el angular CLI

## Instalación

 * descargar el repositorio y descomprimir
 * navegar en la CMD hasta el directorio del proyecto y ejecutar el comando 

 > `ng serve --open`

 con esto se abrirá una ventana en el navegador y se verá el demo.
